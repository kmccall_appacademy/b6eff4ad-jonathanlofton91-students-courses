class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @courses, @first_name, @last_name = [], first_name, last_name
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    if self.courses.include?(new_course)
      return "error"
    else
      self.courses << new_course
      new_course.students << self
    end
  end

  def course_load
    hash = Hash.new(0)
    i = 0
    until i == courses.length
      hash[courses[i].department] += courses[i].credits
      i += 1
    end
    hash
  end

end
